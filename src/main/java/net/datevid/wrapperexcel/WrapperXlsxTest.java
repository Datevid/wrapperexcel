/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.datevid.wrapperexcel;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author doctor
 */
public class WrapperXlsxTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InvalidFormatException, ParseException {
        WrapperXlsx wrapperXlsx = new WrapperXlsx();
        List<Employee> empleados = wrapperXlsx.getData();
        String[] headers = wrapperXlsx.getHeaders();
        byte[] file = wrapperXlsx.generateXlsx(empleados, headers);
        wrapperXlsx.saveFile(file,"reporteBean.xlsx");

    }
    
}
