/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.datevid.wrapperexcel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Datevid
 * Wrapper para generar reportes en xlsx usando apache poi
 * Recepciona un bean formateado y como resultado un array de bytes o un archivo xlsx
 * gracias a: https://www.callicoder.com/java-write-excel-file-apache-poi/
 */
public class WrapperXlsx {

    public String[] getHeaders(){
        String[] columns = {"Nº", "Nombres", "Email", "Fecha Nacimiento", "Salario","Ingreso Institucion"};
        return columns;
    }

    // Initializing employees data to insert into the excel file
    public List<Employee> getData() {
        List<Employee> employees =  new ArrayList<>();

        Calendar dateOfBirth = Calendar.getInstance();
        dateOfBirth.set(1992, 7, 21);
        employees.add(new Employee(null, "rajeev@example.com",dateOfBirth.getTime(), 1200000.0));

        dateOfBirth.set(1965, 10, 15);
        employees.add(new Employee("Thomas cook", "thomas@example.com",dateOfBirth.getTime(), 1500000.0));

        dateOfBirth.set(1987, 4, 18);
        employees.add(new Employee("Steve Maiden", "steve@example.com",dateOfBirth.getTime(), 1800000.0));

        employees.add(new Employee("David León", "datevid@gmail.com", new Date(), 150, "2020-05-06 16:24"));

        return employees;
    }

    public byte[] generateXlsx(List<Employee> employees, String[] headers) throws IOException, ParseException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Data");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 11);
        headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for(int i = 0; i < headers.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle formatoFecha = workbook.createCellStyle();
        formatoFecha.setDataFormat(createHelper.createDataFormat().getFormat("yyyy-MM-dd hh:mm"));

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for(Employee employee: employees) {
            Row row = sheet.createRow(rowNum);

            Cell cell1 = row.createCell(0);
            cell1.setCellValue(rowNum);

            Cell cell2 = row.createCell(1);
            cell2.setCellValue(employee.getName());

            Cell cell3 = row.createCell(2);
            cell3.setCellValue(employee.getEmail());


            Cell cell4 = row.createCell(3);
            cell4.setCellStyle(formatoFecha);
            cell4.setCellValue(employee.getDateOfBirth());


            Cell cell5 = row.createCell(4);
            cell5.setCellValue(employee.getSalary());

            Cell cell6 = row.createCell(5);
            cell6.setCellValue(employee.getDateString());

            rowNum++;

        }

        // Resize all headers to fit the content size
        for(int i = 0; i < headers.length; i++) {
            sheet.autoSizeColumn(i);
        }

        workbook.write(baos);
        workbook.close();
        byte[] xlsx = baos.toByteArray();
        return xlsx;

    }

    public void saveFile(byte[] file, String name) throws IOException {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HHmmss");
        String strDate = dateFormat.format(date);

        FileOutputStream fileOut = new FileOutputStream(name+strDate+".xlsx");
        fileOut.write(file);
        fileOut.close();

    }
}